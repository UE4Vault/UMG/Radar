###### `Note:` This plugin requires the [ALTFunctionLibrary](https://gitlab.com/UE4Vault/ALTFunctionLibrary) to work properly.

## Setup
1. Add a SceneCaptureComponent2D called "Minimap Capture" to your player character.
2. Position Minimap Capture far above the character. The original had 1000m as it's Z Axis Location. Also make sure it faces down (-90 Y Rotation).
3. In the details pane of Minimap Capture, set the Texture Target to "RT_Minimap" (Under the Scene Capture tab)
4. Under the UMG widget named Marker, connect "Hide Actor Components" to Event Construct, and fill the target pin with a reference to Minimap Capture.
5. Add the Minimap UMG widget to another widget positioned where you want it.
6. Create the widget, add it to the viewport, and enjoy!

## How to Use
##### Tracking
* Add the TrackingComponent to actors that you want tracked within the level. Then set the desired variables and they will be tracked.

##### Sonar Projectile
* Use this as a projectile, fire it, and it will show any actors that it moves over, that have the "Discoverable" TrackingComponent trait.

`Note:` If the markers are being tracked incorrectly, there might be an issue inside the UMG widget named Minimap under the function "GetMarkerLocation"

## Current Version
`Unreal Engine 4.19`